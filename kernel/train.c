
#include <kernel.h>

//**************************
//run the train application
//**************************

WINDOW* train_wnd;

 int CONFIG_1 = 1;
 int CONFIG_2 = 2;
 int CONFIG_3 = 3;
 int CONFIG_4 = 4;

 int CHECK_FOR_ZAMBONI = 40;
 int SLEEP_TIME = 15;
 char TRAIN_ID[] = "20";
 int WAIT_TIME = 50; 
 int TRAIN_RETURN_WAIT = 10;

/* converts two digit integer to string */ 
void to_char(int i, char* str) {
  int digit = i % 10;  // units place
  *str = (i/10) + '0';  
  *(str+1) = digit + '0'; // tenths place
}

/* sends given command to com_port */
void send_command(char* command, int length, char* buffer) {
  
  COM_Message msg;
  msg.output_buffer = command; // sets output buufer to command
  if(length != 0){              
    msg.input_buffer = buffer;  // sets input buffer to accept command output 
  }
  msg.len_input_buffer = length; //sets length
  send (com_port, &msg);       //send command to com port
}

/* calls send command and waits for some time 
   before accepting new command*/
void call_command(char *command, int length, char* buffer) {
  send_command(command,length,buffer);
  sleep(SLEEP_TIME);
}

/* clears memory buffer */
void clear_buffer() {
  call_command("R\015",0,0);
}

/* changes position of switch with number 'num' to position 'color' */ 
void change_switch(char num,char color) {
  char command[8] = "M00\015";
  command[1] = num;     // set number of switch
  command[2] = color;   // set posotion represented as color 
  call_command(command,0,0); 
}

/* sets train speed to value passed as an argument */ 
void set_train_speed(char* speed) {
  char command[] = "L20S0\015";
  command[1] = TRAIN_ID[0];    // set train ID as two characters
  command[2] = TRAIN_ID[1];
  command[4] = *speed;
  call_command(command,0,0);
}  

/* starts train with speed 5 */
void train_start() {
   set_train_speed("5");
}

/* stops train */
void train_stop() {
   set_train_speed("0");
}

/* changes direction of train */
void train_change_direction() {
  char command[] = "L00D\015";
  command[1] = TRAIN_ID[0];  // sets train ID as two characters
  command[2] = TRAIN_ID[1];
  call_command(command,0,0);
}

/* probes segment to check existence of an object
   return TRUE if found, otherwise returns FALSE */ 
 BOOL probe_segment(char* segment) {

  char buffer[] = "C00\015";
  char output[3];
  buffer[1]= *segment;          // sets segment to be probed as two characters
  buffer[2] = *(segment+1); 
  clear_buffer();
 call_command(buffer,3,output);
  
  if(output[1] == '1'){
    return TRUE;
  }
   return FALSE;
 }

/* closes all the swicthes to form a closed loop of train track */
void close_outer_loop() {
 change_switch('9','R');
 change_switch('1','G');
 change_switch('4','G');
 change_switch('5','G');
 change_switch('8','G');
}

/* traps zamboni in inner loop in clockwise direction */
void trap_zamboni_clockwise() {
    while(! probe_segment("13"));
   change_switch('9','R');
   change_switch('1','R');
   change_switch('2','R');
   change_switch('7','R');
   change_switch('8','R');

}

/* traps zamboni in inner loop in anticlockwise direction */
void trap_zamboni_anticlockwise() {
    while(! probe_segment("14"));
   change_switch('8','R');
   change_switch('7','R');
   change_switch('2','R');
   change_switch('1','R');
   change_switch('9','R');
}

/* handles common part of configs 1 or 2 */
void handle_config_common() {

   change_switch('6','R');
   change_switch('5','R');
   change_switch('4','R');
   change_switch('3','G');

 sleep(15);
 train_start();
 while(! probe_segment("06"));
 set_train_speed("4");
 while(! probe_segment("01"));
 train_stop();
 train_change_direction();
 train_start();
 while(! probe_segment("08"));
 sleep(TRAIN_RETURN_WAIT); 
 train_stop();

}

/* handles configuration 1 */
void handle_config_1(BOOL zamboni_present) {
  output_string(train_wnd, "\nhandling config 1");

  // trap zamboni in inner loop 
  if(zamboni_present)
     trap_zamboni_clockwise();
  handle_config_common();  
}

/* handles configuration 2 */
void handle_config_2(BOOL zamboni_present) {

  output_string(train_wnd, "\nhandling config 2");
  // trap zamboni in inner loop
  if(zamboni_present)
     trap_zamboni_anticlockwise();
  handle_config_common();  
}

/*  gets train to wagon in configuration 3 */
void get_wagon_config_3(BOOL zamboni_present) {
  change_switch('3','R');
  change_switch('4','R');
  change_switch('2','R');
  change_switch('7','R');

  // wait for zamboni to pass
  if(zamboni_present){
    while(! probe_segment("07"));
   }
  train_start();
  if(zamboni_present){
    while(!probe_segment("13"));
    while(probe_segment("13"));
  }
  
  while(! probe_segment("13"));
  train_stop();
  change_switch('8','R');
  train_change_direction();
  train_start();
  // get wagon on segment 11  
  while(! probe_segment("12"));
  change_switch('8','G');
  train_stop();

}

/* once wagon is attached, gets train to original position in config 3 */
void return_to_origin_config_3(BOOL zamboni_present) {
   // wait for zamboni to pass segment 3
  if(zamboni_present){
    output_string(train_wnd,"\nWaiting for zamboni to pass");
     while(! probe_segment("03"));
  }

  change_switch('3','G');
  change_switch('1','R');
  train_start(); 
  while(! probe_segment("15")); 
  train_stop();
  train_change_direction();
  change_switch('2','G');
  train_start();
  while(! probe_segment("01"));
  change_switch('1','G');
 
 // wait for zamboni to pass segment 7
  if(zamboni_present){
     while(! probe_segment("02"));
     train_stop();
    output_string(train_wnd,"\nWaiting for zamboni to pass");
     while(! probe_segment("07"));
     train_start();	
  }

  change_switch('4','R');
  while(! probe_segment("06"));
  sleep(WAIT_TIME);
  train_stop();
  train_change_direction();
  change_switch('3','R');
  train_start();
  while(! probe_segment("05"));
  sleep(TRAIN_RETURN_WAIT);
  change_switch('4','G');
  train_stop();   // stop after retuning to original position
}

/* handles configuration 3 */
void handle_config_3(BOOL zamboni_present) {

  output_string(train_wnd, "\nhandling config 3");
  get_wagon_config_3(zamboni_present);
  return_to_origin_config_3(zamboni_present);
}

/* gets train to wagon in config 4 */
void get_wagon_config_4(BOOL zamboni_present){

  // wait till zamboni reaches segment 3
  if(zamboni_present){
     while(! probe_segment("03"));
    sleep(WAIT_TIME);
}
 // start train to reach segment 1
  train_start();
  while(! probe_segment("06"));
   sleep(WAIT_TIME);
   train_stop();
   train_change_direction();
   change_switch('3','G');
   change_switch('4','R');
   train_start();
   while(! probe_segment("02"));
   change_switch('4','G');
   // wait till zamboni reaches segment 13
   if(zamboni_present){ 
      output_string(train_wnd,"\nWaiting for zamboni to pass");
      while(! probe_segment("01"));
      train_stop();
       while(! probe_segment("13"));
      train_start(); 
  }
  while(! probe_segment("14"));
  sleep(WAIT_TIME);
  train_stop();
  train_change_direction();

   change_switch('9','G');
   train_start();
   sleep(700);
 // stop when wagon is found on segment 16
 train_stop(); 
}

/* once wagon is attached, gets train to its original position in config 4 */
void return_to_origin_config_4(BOOL zamboni_present) {
 
train_change_direction();
  // wait till zamboni reaches segment 13
  if(zamboni_present) {
     while(! probe_segment("13"));
  }
  // start train till it reaches segment 6
 train_start();
  while(! probe_segment("14"));
  sleep(WAIT_TIME);
  train_stop();
  train_change_direction();
  change_switch('9','R');
  change_switch('1','R');
  change_switch('2','G');
  change_switch('3','G');
  train_start();

   // wait till zamboni to reach segment 4
   if(zamboni_present) {
     while(! probe_segment("01"));
     change_switch('1','G');
     while(! probe_segment("02"));
     train_stop();
     output_string(train_wnd,"\nWaiting for zamboni to pass");
     while(! probe_segment("04"));
    train_start();
  }

  // continue till train reaches segment 6
  change_switch('4','R');
  while(! probe_segment("06"));
  sleep(WAIT_TIME);
  train_stop(); 
  // change direction and continue till train reaches segment 5
  change_switch('3','R');
  train_change_direction();
  train_start();
  while(! probe_segment("05"));
  sleep(TRAIN_RETURN_WAIT);
  train_stop();
 change_switch('4','G');
}

/* handles config 4 */
void handle_config_4(BOOL zamboni_present) {

  output_string(train_wnd, "\nhandling config 4");
  get_wagon_config_4(zamboni_present);
  return_to_origin_config_4(zamboni_present);

}

/* checks for configuration based on the positions of train and wagon
   and returns the detected configuration number */
int check_configuration() {
    output_string(train_wnd,"\nchecking configuration");
   if(probe_segment("08")) {
      if(probe_segment("02"))
        return CONFIG_1;
  } else{
     if(probe_segment("05")) {
         if(probe_segment("11")){
            return CONFIG_3;
        } else {
            if(probe_segment("16")){
              return CONFIG_4;
            } 
        }
     }
  }  

 return 1;
} 

/* chcks if zamboni is present or not. 
   returns TRUE, if zamboni is found, else returns FALSE */
BOOL check_zamboni() {
  int i;
  
 output_string(train_wnd,"\n Checking for Zamboni"); 
 for( i=0; i < CHECK_FOR_ZAMBONI;i++){
   if(probe_segment("06"))
     return TRUE;
   sleep(100);
 }
 return FALSE;


}

/* depending on direction of zamboni, checks if the configurations is 1 or 2 */
int confirm_configuration(){
 while(! probe_segment("10"));
 while(probe_segment("10"));
 if(probe_segment("13") == 1)
  return 1;
  
 return 2;
}

/* starts train application */
void train() {
 close_outer_loop();
 int configuration = check_configuration();
 BOOL zamboni_present = check_zamboni(); 

  // check if zamboni is present 
 if(zamboni_present)
   output_string(train_wnd,"\n Zamboni Present!");
 else
   output_string(train_wnd,"\n Zamboni Not detected!");

 // checks if configuration is 1 or 2 depending on direction of zamboni
 if(zamboni_present && configuration == 1)
    configuration = confirm_configuration();

  // based on configuration, call appropriate function 
 switch(configuration) {
  case 1:
       handle_config_1(zamboni_present);
       break;
  case 2:
       handle_config_2(zamboni_present);
       break;

  case 3:
       handle_config_3(zamboni_present);
       break;

  case 4:
       handle_config_4(zamboni_present);
       break;
 }
  output_string(train_wnd,"\n Safely returned with Wagon");
}

/* proces to handle train application */
void train_process(PROCESS self, PARAM param)
{
  clear_buffer();
  clear_window(train_wnd);
   train();
   while(1);
}

/* creates a process with priority 3 to handle train application */
void init_train(WINDOW* wnd)
{
   create_process(train_process,3,0,"train process");  
   train_wnd = wnd;
}

