#include <kernel.h>

WINDOW shell_wnd_def = {0, 9, 61, 16, 0, 0, 0xDC};
WINDOW pacman_wnd_def = {61,9 ,80, 23, 0, 0, 0};
WINDOW train_wnd = {0, 0, 80, 8, 0, 0, ' '};

WINDOW* shell_window = &shell_wnd_def;
int COMMAND_SIZE = 70;
BOOL pacman_started = FALSE;
BOOL train_started = FALSE;  
  
   /* Prints given string on shell */
void shell_print(char * str) {
   output_string(shell_window,str);
}
  
   /* Prints error messsage on shell */
void shell_print_error(char *error) {
    shell_print(error);
    shell_print("Try \"help\" for more details\n"); 
}


   /* Compares two string. If they are equal, returns true, else returns false. */
BOOL strcompare(char* str1, char* str2){

    while(*str1 != '\0' && *str2 != '\0') {        
       if(*str1 != *str2){
          return FALSE;
       }
       str1++;
       str2++;   
    }

   if(*str1 != '\0' || *str2 != '\0')
       return FALSE;
  
   return TRUE;
}
  
  /* Converts string to integer.Returns -1 in case of negative numbers or invalid strings */
int to_integer(char a[]) {
    int c, n;
   n = 0;
 
  for (c = 0; a[c] != '\0'; c++) {
    if(a[c] < 48 ||  a[c] > 57){
       return -1; 
    }
    n = n * 10 + a[c] - '0';
  }
  return n;
}

   /* Executes help command: display help */
 void execute_help(int no_of_arguments) {
     if(no_of_arguments != 0) {
         shell_print_error("Invalid number of arguments for help command.\n");
         return;
     }
     shell_print("Command  Description \n");
     shell_print("----------------------------------------------\n");
     shell_print("help            Displays list of avalilable commands\n");
     shell_print("ps              Displays list of processes \n");
     shell_print("pacman <ghosts> Starts Pacman game with ghosts = <ghosts>\n");
     shell_print("clear           Clears the shell window \n");
     shell_print("about           Displays information about author\n");
     shell_print("echo  <string>  Displays input <string> on shell\n");
     shell_print("train start     Starts the train\n");
     shell_print("train stop      Stops the train\n");
     shell_print("train reverse   Changes the direction of train\n"); 
     shell_print("                Note:To change direction,first stop the train");
     shell_print("train           Runs train application\n"); 
}

   /* Executes about command: display author information */
void execute_about(int no_of_arguments) {
    if(no_of_arguments != 0){
       shell_print_error("Invalid number of arguments for about command.\n");
       return;
    }

    shell_print("          Author's information\n"); 
    shell_print("--------------------------------------------\n");
    shell_print("Author:     Pooja Kanchan\n");
    shell_print("University: San Francisco State University\n");
    shell_print("Year:       April 2016\n");

}

   /* Executes ps command: prints processes */
void execute_ps(int no_of_arguments) {
    if(no_of_arguments != 0){
       shell_print_error("Invalid number of arguments for ps command.\n");
       return;
    }
    print_all_processes(shell_window);
}

  /* Executes pacman command: start pacman game */
void execute_pacman(WINDOW* wnd, int number_of_ghosts) {
     
   if(number_of_ghosts <= 0) {        // check number of arguments
      shell_print_error("Invalid argument for pacman command.\n");
      return;
   } 
   
   if(pacman_started){              // check if pacman has already been started
     shell_print("Pacman is already running\n");
     return;
   }

   if(number_of_ghosts > 6) {      // check if number of ghosts are within range
      shell_print("Please enter number of ghosts between 1 to 6\n");
      return;
   }
   pacman_started = TRUE;            // set the flag
   init_pacman(wnd,number_of_ghosts); // start the game
   shell_print("Pacman started!!\n");
}

  /*  Executes clear command: clear the shell window */
void execute_clear(int no_of_arguments){

   if(no_of_arguments != 0){       // check number of arguments
      shell_print_error("Invalid number of arguments for clear command.\n");
      return;
   }
   clear_window(shell_window);     // call clear_window function
   shell_print("****** TOS SHELL *********\n\n");
}
  
  /* Gets the next token from string , ignores white spaces in the beginning*/
void get_next_token(char * command_line, char* command,int* ind) {

   int index = *ind;
   while(command_line[index] == 32) {  // trim the spaces in the beginning
       index++;
   }
  
   while(command_line[index] != '\0' && command_line[index] != 32) { // get the token
       *command = command_line[index];
       command++;
       index++; 
   }
   *command = '\0';
   *ind = index;
}

  /* Calculates number of arguments in command separated by white spaces */
int get_no_of_arguments(char *command_line, int index) {
   int no_of_arguments = 0; 

   while(command_line[index] != '\0') {    
      if(command_line[index] == 32) {     // when one or more white spaces are found, increment count       
           while(command_line[index] == 32){
               index++;  
           }  
          no_of_arguments++;
      } else {
          index++;        
      }  
   } 
   return no_of_arguments;
}  

   /* Executes echo command: echo string */
void execute_echo(char* command_line, int index) {
    if(command_line[index] == 32) {     // trims white spaces in the beginning
       while(command_line[index] == 32){
           index++;
       }  
    }  
    shell_print(&command_line[index]);   // print the passed string
    shell_print("\n");
}

 /* Sends command to com port */
void send_train_command(char* command) {
  COM_Message msg;
  msg.output_buffer = command;
  msg.len_input_buffer = 0;
  send (com_port, &msg);
}

  /* Executes train command */
void execute_train(char* argument) {
 
  if(strcompare(argument,"start")) {        // start command
     if(train_started) {          // if train is already moving, display error message and exit
         shell_print("Train has already been started.\n");
         return;
      }
      send_train_command("L20S4\015");    // start train
      shell_print("Train started!!\n");
      train_started = TRUE;
  } else if(strcompare(argument,"stop")) {   // stop command
     if(! train_started) {                  // iftrain is already at stop, display error message and exit
        shell_print("Train is already at stop.\n");
        return;
      }
      send_train_command("L20S0\015");      // stop the train
      shell_print("Train stopped!!\n");
      train_started = FALSE;
   } else if(strcompare(argument,"reverse")) {
        if(train_started) {          // if train is already moving, display error message and exit
         shell_print("Please stop the train first to avoid abortion!!\n");
         return;
        }  

      send_train_command("L20D\015");     // change direction of train
      shell_print("Direction of train changed!!\n");
    } else {
       shell_print_error("Invalid argument for train command\n");
    }   
}


  /* Parses given command, validates it and executes if valid. 
     If not valid, prints appropriate error message. */
void execute_command(char* command_line) {
   char command[COMMAND_SIZE];
   char argument[COMMAND_SIZE];  
   int index =0;     

   // fetch command from command line
   get_next_token(command_line,command,&index);
   // get number of arguments
   int no_of_arguments = get_no_of_arguments(command_line,index);
  
   // compare command and execute
   if(strcompare("help",command)) {   

       execute_help(no_of_arguments);    // execute <help>

   } else if(strcompare("ps",command)){  

       execute_ps(no_of_arguments);    // execute <ps>

   } else if(strcompare("pacman",command)) {
       if(no_of_arguments != 1){
           shell_print_error("Invalid number of arguments for pacman command.\n");
           return;
       }
      // get the arguemnt
      get_next_token(command_line,argument,&index);
      // convert the argument to intger
      int arg = to_integer(argument);
      execute_pacman(&pacman_wnd_def,arg);   // execute <pacman>

   } else if(strcompare("clear",command)) { 

       execute_clear(no_of_arguments);     // execute <clear>

   } else if(strcompare("about",command)){

       execute_about(no_of_arguments);     // execute <about>

   } else if(strcompare("echo",command)) {
       execute_echo(command_line,index);   // execute <echo>

   } else if(strcompare("train", command)) {  
     
     if(no_of_arguments != 1){
           wprintf(&shell_wnd_def,"%u\n",&train_wnd);
           init_train(&pacman_wnd_def);
       } else{
      // get the arguemnt
        get_next_token(command_line,argument,&index);
        execute_train(argument);    // execute <train>
     } 
   } else{
       shell_print_error("Invalid command. ");  // print error message, if the command not found
       return;
   }
  
}

 /* Reads command input character by character and saves it in buffer.
  When <enter> is pressed, sends it for execution. */

void shell_process(PROCESS self, PARAM param)
{
   execute_clear(FALSE);
   Keyb_Message msg;
   char command[COMMAND_SIZE];
   char ch;

  // endless loop
  while (1) {
     int index =0; 
     shell_print(">>");
     BOOL read_command = TRUE;
     // loop to get a command
     while(read_command) {
       msg.key_buffer = &ch;
       send(keyb_port, &msg);
  
       switch(ch) {         
          case '\b':         // handle <backslash>
             if(index > 0) {    // check for buffer underflow
               index--;
               output_char(shell_window, ch);           
              }  
              break;

          case 13:          // handle <enter>
             if(index > 0) { 
               int current_index = index -1;
               while(command[current_index] == 32) {  // trim spaces in the end
      	         current_index--;
               }   
               index = current_index +1;
             }
             output_char(shell_window, ch);
             read_command = FALSE; 
             break;
 
          default:
            if(index == COMMAND_SIZE -1)    // check for buffer oveflow
              continue;
            output_char(shell_window,ch);
            command[index] = ch;
            index++;
            break;
     } // close switch

    }  // inner loop

  // in case of blank output, continue
   if(index == 0)
       continue;
   // append '\0' at the end
   command[index] = '\0';

   // execute entered command 
   execute_command(command); 
  } // outer loop
}

 /* Creates shell process with priority: 5 and name: "Shell_Process" */
void init_shell()
{
    create_process(shell_process, 5, 0, "Shell_Process");    
}



